-- Advogados / Advocacia --

- Produtos deste Repositório
-- CakePHP versao 2.6
-- Plugins
--- Admin - Sistema Advocacia
--- API - API Restful
--- Bootstrap - Carrega Twitter Bootstrap CSS

- Requerimentos
-- Servidor Web Apache com mod-rewrite habilitado
-- PHP versão 5.5
-- Banco de dados PostgreSQL
-- GIT

- Instalação

- Clonar o respositório em um diretório vazio:
git clone git@gitlab.com:advogados/advocacia.git .

- Configurar o "root" do Servidor Web para a pasta /app/webroot no diretório onde foi feito o clone
- Copiar o arquivo /app/Config/database.php.default para /app/Config/database.php e edita-lo de acordo 
que configuração $default com os parâmetros de acesso do Banco de Dados
- O arquivo /app/Config/core.php possui as variáveis Security.salt e Security.cipherSeed que devem ser
alteradas para outro valor. Estas variáveis são utilizadas para a geração de hash para criptografia 
para as senhas dos usuário. Uma vez alterado esses valores, qualquer tentativa de login não poderá ser
feita. Novas senhas deveram ser criadas para cada usuário.

- Banco de Dados
-- Uma cópia do Banco de Dados deve ser criada e sua estrutura modificada 
de acordo com as especificações da estrutura proposta neste projeto.

- Atualiação
-- Os arquivos do sistema ainda estão sendo atualizados praticamente todos os dias, é aconselhavel 
criar um CRONJOB progamado para atualizar todo o sistema todas as manhãs:

git pull origin master

- Para modificações mais complexas que podem ser necessárias será criada uma nova branch do GIT, desta 
forma, atualizações sempre feitas na branch "master" não irá causar problemas no uso normal do sistema, 
apenas adição de novas funcionalidades.
