<?php
class RetornosController extends ApiAppController {

	public $components = array('RequestHandler');
	public $uses = array('Api.Retorno');
	
	public function delete($id = null) {
		
		$this->Retorno->BoletoNovoRetorno->deleteAll(
			array('BoletoNovoRetorno.retorno_id' => $id)
		);
		
		$this->Retorno->delete($id);
		
		unlink(APP.'retornos/retorno_'.$id.'.ret');
		
		$data = array(
			'msg' => 'Registro excluído com sucesso!',
			'error' => 0
		);
		
		$this->set('data',$data);
		$this->set('_serialize', array('data'));
	}

}

