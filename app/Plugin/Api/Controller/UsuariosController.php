<?php
class UsuariosController extends ApiAppController {

	public $components = array('RequestHandler');
	public $uses = array('Api.Usuario');
	
	public function add() {
		$data = $this->request->data;
		if (isset($data['senha']))
			$this->request->data['password'] = $this->Auth->password($data['senha']);
		
		parent::add();
	}
	
	public function edit($id = null) {
		$data = $this->request->data;
	
		if (isset($data['senha']))
			$this->request->data['password'] = $this->Auth->password($data['senha']);
		
		parent::edit();
		
	}

}

