<?php
class BoletosnovosController extends ApiAppController {

	public $components = array('RequestHandler');
	public $uses = array('Api.BoletoNovo');
	
	public function total_atrasado() {
		$total = $this->BoletoNovo->find('all', array(
			'fields' => array(
				'sum(BoletoNovo.valor) AS total'
			),
			'conditions' => array(
				'BoletoNovo.situacao_id' => 1,
				'BoletoNovo.data_vencimento <' => date('Y-m-d')
			)
		));
		$this->set('data', $total);
		$this->set('_serialize', array( 'data' ) );
	}

}

