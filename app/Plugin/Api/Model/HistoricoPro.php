<?php

class HistoricoPro extends ApiAppModel {

	public $useTable = 'sisadv_historicopro';
	public $primaryKey = 'id';
	public $order = array(
		'HistoricoPro.id' => 'DESC'
	);
	
	public $belongsTo = array(
		'Sacado' => array(
			'className' => 'Api.Sacado',
			'foreignKey' => 'sacado_id'
		),
		'Usuario' => array(
			'className' => 'Api.Usuario',
			'foreignKey' => 'user_id'
		)
	);
	
	public $hasMany = array(
		'Arquivo' => array(
			'className' => 'Api.Arquivo',
			'foreignKey' => 'historicopro_id'
		)
	);

}