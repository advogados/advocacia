<?php

class Grupo extends ApiAppModel {

	public $useTable = 'grupos';
	public $useDbConfig = 'auth';
	public $primaryKey = 'id';
	
	public $hasManu = array(
		'Usuario' => array(
			'className' => 'Api.Usuario',
			'foreignKey' => 'grupo_id'
		)
	);
	
}