<?php

class Usuario extends ApiAppModel {

	public $useTable = 'usuarios';
	public $useDbConfig = 'auth';
	public $primaryKey = 'id';
		
	public $belongsTo = array(
		'Grupo' => array(
			'className' => 'Api.Grupo',
			'foreignKey' => 'grupo_id'
		)
	);
	
	public $hasMany = array(
		'HistoricoAdm' => array(
			'className' => 'Api.HistoricoAdm',
			'foreignKey' => 'user_id'
		),
		'HistoricoPro' => array(
			'className' => 'Api.HistoricoPro',
			'foreignKey' => 'user_id'
		)
	);
	
	var $controllerAction = null;

	function setControllerAction( $action = null ) {
		if($action) {
			$this->controllerAction = $action;
		}
	}
	
	public function afterFind($results, $primary = false) {
		
		if($this->controllerAction != 'login') {
			foreach ($results as $key => $val) {
				if (isset($val['Usuario']['last_login']))
					$results[$key]['Usuario']['last_login'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Usuario']['last_login']));
				if (isset($val['Usuario']['date_joined']))
					$results[$key]['Usuario']['date_joined'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Usuario']['date_joined']));
				if (isset($val['Usuario']['old_password']))
					unset($results[$key]['Usuario']['old_password']);
				if (isset($val['Usuario']['password']))
					unset($results[$key]['Usuario']['password']);
			}
		}
		return $results;
		
	}

}