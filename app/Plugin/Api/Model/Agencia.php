<?php

class Agencia extends ApiAppModel {

	public $useTable = 'sisadv_agencia';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Banco' => array(
			'className' => 'Api.Banco',
			'foreignKey' => 'banco_id'
		)
	);
	
	public $hasMany = array(
		'Conta' => array(
			'className' => 'Api.Conta',
			'foreignKey' => 'agencia_id'
		)
	);

}