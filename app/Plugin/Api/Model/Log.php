<?php

class Log extends ApiAppModel {

	public $useTable = 'logs';
	public $useDbConfig = 'auth';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Usuario' => array(
			'className' => 'Api.Usuario',
			'foreignKey' => 'usuario_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($val['Log']['data']))
				$results[$key]['Log']['data'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Log']['data']));
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['Usuario']['data']) ) {
			$this->data['Usuario']['data'] = date_format(date_create_from_format('Y-m-d\TH:i:s-0300', $this->data['Usuario']['data'] ), 'Y-m-d H:i:s' );
		}
		return true;
	}
	
}