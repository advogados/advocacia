<?php

class SituacaoBoleto extends ApiAppModel {

	public $useTable = 'sisadv_situacaoboleto';
	public $primaryKey = 'id';
	
	public $hasMany = array(
		'BoletoAntigo' => array(
			'className' => 'Api.BoletoAntigo',
			'foreignKey' => 'situacao_id'
		),
		'BoletoNovo' => array(
			'className' => 'Api.BoletoNovo',
			'foreignKey' => 'situacao_id'
		)
	);
	
}