<?php

class BoletoAntigo extends ApiAppModel {

	public $useTable = 'sisadv_boletoantigo';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Sacado' => array(
			'className' => 'Api.Sacado',
			'foreingKey' => 'sacado_id'
		),
		'SituacaoBoleto' => array(
			'className' => 'Api.SituacaoBoleto',
			'foreignKey' => 'situacao_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		
		foreach ($results as $key => $val) {
			if (isset($val['BoletoAntigo']['data_vencimento']))
				$results[$key]['BoletoAntigo']['data_vencimento'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoAntigo']['data_vencimento']));
			if (isset($val['BoletoAntigo']['data_pagamento']))
				$results[$key]['BoletoAntigo']['data_pagamento'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoAntigo']['data_pagamento']));
			if (isset($val['BoletoAntigo']['exc_data']))
				$results[$key]['BoletoAntigo']['exc_data'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoAntigo']['exc_data']));
		}
		return $results;
		
	}

}