<?php

class Documento extends ApiAppModel {

	public $useTable = 'sisadv_documento';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Outorgante' => array(
			'className' => 'Api.Sacado',
			'foreignKey' => 'outorgante_id'
		),
		'Representante' => array(
			'className' => 'Api.Sacado',
			'foreignKey' => 'representante_id'
		)

	);
	
	public $hasMany = array(
		'BoletoNovo' => array(
			'className' => 'Api.BoletoNovo',
			'foreignKey' => 'documento_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		
		foreach ($results as $key => $val) {
			if (isset($val['Documento']['data']))
				$results[$key]['Documento']['data'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Documento']['data']));
		}
		return $results;
		
	}

}