<?php

class Banco extends ApiAppModel {

	public $useTable = 'sisadv_banco';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		
	);
	
	public $hasOne = array(
		
	);
	
	public $hasMany = array(
		'Agencia' => array(
			'className' => 'Api.Agencia',
			'foreignKey' => 'banco_id',
			'order' => array(
				'Agencia.nome' => 'ASC'
			)
		),
	);

	/*
	public function afterFind($results, $primary = false) {
			foreach ($results as $key => $val) {
			if (isset($results[$key]['Retorno'])) {
				$results[$key]['Retorno']['data_envio'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Retorno']['data_envio']));
			}
		}
		return $results;
	}
	*/

}