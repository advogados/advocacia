<?php

class BoletoNovoRetorno extends ApiAppModel {

	public $useTable = 'sisadv_boletonovo_retorno';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Retorno' => array(
			'className' => 'Api.Retorno',
			'foreingKey' => 'retorno_id'
		),
		'BoletoNovo' => array(
			'className' => 'Api.BoletoNovo',
			'foreignKey' => 'boletonovo_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		
		foreach ($results as $key => $val) {
			if (isset($val['BoletoNovoRetorno']['data_pagamento']))
				$results[$key]['BoletoNovoRetorno']['data_pagamento'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoNovoRetorno']['data_pagamento']));
		}
		return $results;
		
	}

}