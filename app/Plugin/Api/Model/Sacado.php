<?php

class Sacado extends ApiAppModel {

	public $useTable = 'sisadv_sacado';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'EstadoCivil' => array(
			'className' => 'Api.EstadoCivil',
			'foreignKey' => 'estadocivil_id'
		)
	);
	
	public $hasMany = array(
		'BoletoAntigo' => array(
			'className' => 'Api.BoletoAntigo',
			'foreignKey' => 'sacado_id',
			'order' => array(
				'BoletoAntigo.data_vencimento' => 'ASC'
			)
		),
		'BoletoNovo' => array(
			'className' => 'Api.BoletoNovo',
			'foreignKey' => 'sacado_id',
			'order' => array(
				'BoletoNovo.data_vencimento' => 'ASC'
			)
		),
		'HistoricoAdm' => array(
			'className' => 'Api.HistoricoAdm',
			'foreignKey' => 'sacado_id',
			'order' => array(
				'HistoricoAdm.id' => 'DESC'
			)
		),
		'HistoricoPro' => array(
			'className' => 'Api.HistoricoPro',
			'foreignKey' => 'sacado_id',
			'order' => array(
				'HistoricoPro.id' => 'DESC'
			)
		)
	);

}