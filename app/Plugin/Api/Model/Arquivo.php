<?php

class Arquivo extends ApiAppModel {

	public $useTable = 'sisadv_arquivos';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'HistoricoAdm' => array(
			'className' => 'Api.HistoricoAdm',
			'foreignKey' => 'historicoadm_id'
		),
		'HistoricoPro' => array(
			'className' => 'Api.HistoricoPro',
			'foreignKey' => 'historicopro_id'
		)
	);
	
}