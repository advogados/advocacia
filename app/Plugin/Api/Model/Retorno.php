<?php

class Retorno extends ApiAppModel {

	public $useTable = 'sisadv_retornos';
	public $primaryKey = 'id';
	
	public $hasMany = array(
		'BoletoNovoRetorno' => array(
			'className' => 'Api.BoletoNovoRetorno',
			'foreignKey' => 'retorno_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			if (isset($results[$key]['Retorno'])) {
				$results[$key]['Retorno']['data_envio'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Retorno']['data_envio']));
			}
		}
		return $results;
		
	}

}