<?php

class Conta extends ApiAppModel {

	public $useTable = 'sisadv_conta';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Agencia' => array(
			'className' => 'Api.Agencia',
			'foreignKey' => 'agencia_id'
		)

	);
	
	public function afterFind($results, $primary = false) {
		
		foreach ($results as $key => $val) {
			if (isset($val['Conta']['data_saldo'])) {
				$results[$key]['Conta']['data_saldo'] = date('Y-m-d\TH:i:s-0300', strtotime($val['Conta']['data_saldo']));
			}
		}
		return $results;
		
	}

}