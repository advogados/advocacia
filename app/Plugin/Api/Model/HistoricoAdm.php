<?php

class HistoricoAdm extends ApiAppModel {

	public $useTable = 'sisadv_historico';
	public $primaryKey = 'id';
	public $order = array(
		'HistoricoAdm.id' => 'DESC'
	);
	
	public $belongsTo = array(
		'Sacado' => array(
			'className' => 'Api.Sacado',
			'foreignKey' => 'sacado_id'
		),
		'Usuario' => array(
			'className' => 'Api.Usuario',
			'foreignKey' => 'user_id'
		)
	);
	
	public $hasMany = array(
		'Arquivo' => array(
			'className' => 'Api.Arquivo',
			'foreignKey' => 'historicoadm_id'
		)
	);

}