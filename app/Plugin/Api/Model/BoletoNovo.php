<?php

class BoletoNovo extends ApiAppModel {

	public $useTable = 'sisadv_boletonovo';
	public $primaryKey = 'id';
	
	public $belongsTo = array(
		'Sacado' => array(
			'className' => 'Api.Sacado',
			'foreingKey' => 'sacado_id'
		),
		'SituacaoBoleto' => array(
			'className' => 'Api.SituacaoBoleto',
			'foreignKey' => 'situacao_id'
		),
		'Documento' => array(
			'className' => 'Api.Documento',
			'foreignKey' => 'documento_id'
		),
		'UserExcl' => array(
			'className' => 'Api.Usuario',
			'foreignKey' => 'exc_user_id'
		)
	);
	
	public $hasMany = array(
		'BoletoNovoRetorno' => array(
			'className' => 'Api.BoletoNovo',
			'foreignKey' => 'boletonovo_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		
		foreach ($results as $key => $val) {
			if (isset($val['BoletoNovo']['data_vencimento']))
				$results[$key]['BoletoNovo']['data_vencimento'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoNovo']['data_vencimento']));
			if (isset($val['BoletoNovo']['data_pagamento']))
				$results[$key]['BoletoNovo']['data_pagamento'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoNovo']['data_pagamento']));
			if (isset($val['BoletoNovo']['exc_data']))
				$results[$key]['BoletoNovo']['exc_data'] = date('Y-m-d\TH:i:s-0300', strtotime($val['BoletoNovo']['exc_data']));
		}
		return $results;
		
	}

}