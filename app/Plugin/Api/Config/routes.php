<?php	
	
	Router::resourceMap( array(
		array( 'action' => 'index', 'method' => 'GET', 'id' => false ),
		array( 'action' => 'view', 'method' => 'GET', 'id' => true ),
		array( 'action' => 'add', 'method' => 'POST', 'id' => false),
		array( 'action' => 'edit', 'method' => 'POST', 'id' => true ),
		array( 'action' => 'delete', 'method' => 'DELETE', 'id' => true ),
	) );
	
	
	Router::mapResources( 
		array(
			'Api.Agencias',
			'Api.Arquivos',
			'Api.Bancos',
			'Api.Boletosnovos',
			'Api.Contas',
			'Api.Documentos',
			'Api.Retornos',
			'Api.Sacados',
			'Api.Textos',
			'Api.Usuarios',
			'Api.Historicoadm',
			'Api.Historicopro'
		)
	);

	Router::parseExtensions();
