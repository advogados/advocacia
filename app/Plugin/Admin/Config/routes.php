<?php
	Router::connect('/admin', array('plugin'=>'admin', 'controller' => 'Home', 'action' => 'index'));
	
	Router::connect('/login', array('plugin'=>'admin', 'controller' => 'Usuario', 'action' => 'login'));
	Router::connect('/logout', array('plugin'=>'admin', 'controller' => 'Usuario', 'action' => 'logout'));
	
	Router::connect('/auth', array('plugin'=>'admin', 'controller' => 'Usuario', 'action' => 'auth'));
	Router::connect(
		'/baixa/:id', 
		array('plugin'=>'admin', 'controller' => 'Retorno', 'action' => 'baixa'), 
		array('pass'=>array('id'))
	);

