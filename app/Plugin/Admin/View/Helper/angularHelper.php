<?php
	class angularHelper extends AppHelper {
		
		var $helpers = array('Html');
		
		public function scripts() {
			// Extenso JS
			echo $this->Html->script('/admin/extenso.js');
			
			echo $this->Html->script('/admin/bower_components/angular/angular.min.js');
			echo $this->Html->script('/admin/bower_components/angular-jquery/dist/angular-jquery.min.js');
			echo $this->Html->script('/admin/bower_components/angular-classy/angular-classy.min.js');
			echo $this->Html->script('/admin/bower_components/classy-on/classy-on.js');
			echo $this->Html->script('/admin/bower_components/angular-route/angular-route.min.js');
			echo $this->Html->script('/admin/bower_components/angular-resource/angular-resource.min.js');
			echo $this->Html->script('/admin/bower_components/angular-animate/angular-animate.min.js');
			echo $this->Html->script('/admin/bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min.js');
			echo $this->Html->script('/admin/bower_components/angular-i18n/angular-locale_pt-br.js');
			
			// Angular Audio
			echo $this->Html->script('/admin/bower_components/angular-audio/app/angular.audio.js');
			
			// Underscore
			echo $this->Html->script('/admin/bower_components/underscore/underscore-min.js');
			
			// ngWig
			echo $this->Html->css('/admin/bower_components/ng-wig/dist/css/ng-wig.css');
			echo $this->Html->script('/admin/bower_components/ng-wig/dist/ng-wig.min.js');
			
			//Affix
			echo $this->Html->script('/admin/bower_components/angular-bootstrap-affix/dist/angular-bootstrap-affix.min.js');
			
			// Bootstrap Switch
			echo $this->Html->css('/admin/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css');
			echo $this->Html->script('/admin/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js');
			echo $this->Html->script('/admin/bower_components/angular-bootstrap-switch/dist/angular-bootstrap-switch.min.js');

			
			// Brasil Filters 
			echo $this->Html->script('/admin/bower_components/angular-br-filters/release/angular-br-filters.min.js');
			
			// Input Mask
			echo $this->Html->script('/admin/bower_components/angular-input-masks/angular-input-masks.br.min.js');
			
			// File Upload
			echo $this->Html->script('/admin/bower_components/angular-file-upload/angular-file-upload.min.js');
						
		}
		
		public function controllers() {
			echo $this->Html->script('Admin./app/app.js');
			echo $this->Html->script('Admin./app/routes.js');
			echo $this->Html->script('Admin./app/dataService.js');
			echo $this->Html->script('Admin./app/modelService.js');
			// App Controllers
			echo $this->Html->script('/admin/app/mainController.js');
						
			// Home
			echo $this->Html->script('/admin/app/Home/index.js');
			
			// Contas
			echo $this->Html->script('/admin/app/Conta/index.js');
			echo $this->Html->script('/admin/app/Conta/form.js');
			
			// Sacados
			echo $this->Html->script('/admin/app/Sacado/index.js');
			echo $this->Html->script('/admin/app/Sacado/form.js');
			echo $this->Html->script('/admin/app/Sacado/view.js');
			echo $this->Html->script('/admin/app/Sacado/docs.js');
			
			// Logs
			echo $this->Html->script('/admin/app/Log/index.js');
			
			// Retornos
			echo $this->Html->script('/admin/app/Retorno/index.js');
			
			// Relatórios
			echo $this->Html->script('/admin/app/Relatorio/index.js');
			
			// Textos
			echo $this->Html->script('/admin/app/Texto/index.js');
			echo $this->Html->script('/admin/app/Texto/form.js');

			// Parcelamentos
			echo $this->Html->script('/admin/app/Parcelamento/index.js');
			echo $this->Html->script('/admin/app/Parcelamento/view.js');
			echo $this->Html->script('/admin/app/Parcelamento/boletos.js');
			echo $this->Html->script('/admin/app/Parcelamento/docs.js');
			echo $this->Html->script('/admin/app/Parcelamento/form.js');
			
			// Usuários
			echo $this->Html->script('/admin/app/Usuario/index.js');
			echo $this->Html->script('/admin/app/Usuario/form.js');
		}
		
	}