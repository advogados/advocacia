<!DOCTYPE html><html><head><title><?php echo $this->fetch('title'); ?></title><?php 
echo $this->tbs->tbsCSS();
echo $this->tbs->tbwCSS();
echo $this->tbs->FontAwesomeCSS();
echo $this->Html->css('Admin.admin');
echo $this->Html->script('Admin.admin');
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script'); 
?></head><body><?php echo $this->Session->flash(); ?>
<?php echo $this->fetch('content'); ?>
<?php echo $this->element('sql_dump'); ?></body></html>