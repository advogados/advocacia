<?php
	class AdminAppController extends AppController {
		
		//public $uses = 'Admin.Log';
		
		var $helpers = array(
			'Admin.angular',
			'Bootstrap.tbs',
			'Html',
			'Form'
		);
		var $layout = 'Admin.theme';
		
		public function index() {
			
		}
		
		public function add() {
			
		}
		
		public function edit($id = null) {
			
		}
		
		public function del($id = null) {

		}

	}