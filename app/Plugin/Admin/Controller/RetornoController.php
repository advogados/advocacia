<?php
	class RetornoController extends AdminAppController {
		
		public $uses = array('Api.Retorno');
		public $components = array('RequestHandler');
		
		public function _baixa($id = null) {
			/*
				if linha[0:1] == '0':
			valores['codigo_retorno'] = linha[1:2]
			valores['literal_retorno'] = linha[2:9]
			valores['codigo_servico'] = linha[9:11]
			valores['literal_servico'] = linha[11:26]
			valores['agencia'] = linha[26:30]
			valores['conta'] = linha[32:37]
			valores['dac'] = linha[37:38]
			valores['nome_empresa'] = linha[46:76]
		if linha[0:1] == '1':
			valores['nosso_numero'] = linha[62:70]
			valores['data_ocorrencia'] = linha[110:116]
			valores['data_vencimento'] = linha[146:152]
			valores['valor'] = linha[152:165]
			dt_pag[int(valores['nosso_numero'])] = valores['data_ocorrencia']
			
			id_encontrados.append(valores['nosso_numero'])
			linhas[i] = valores
			i = i + 1
			*/
			$campos = array(
				array(
					'nosso_numero',
					62,
					8
				),
				array(
					'dt_ocorrencia',
					110,
					6
				),
				array(
					'dt_vencimento',
					146,
					6
				),
				array(
					'valor',
					152,
					13
				)

			);
			
			$retorno = $this->Retorno->read(null, $id);
			$arquivo = APP.'retornos/retorno_'.$id.'.ret';
			$arquivo = file_get_contents($arquivo);
			
			$arquivo = split(chr(10), $arquivo);
			
			$footer = array_pop($arquivo);
			$footer = array_pop($arquivo);
			$header = array_shift($arquivo);
						
			$retorno = array();
			$ids = array();
			foreach($arquivo as $linha) {
				array_push( $ids, intval( substr( $linha, 62, 8 ) ) );
				$documentos = array();
				foreach($campos as $field) {
					$value = substr( $linha, $field[1], $field[2] );
					$documentos[$field[0]] = $value;
				}
				array_push( $retorno, $documentos );
			};

			$data = array();
			$boletos = array();
			foreach($retorno as $ret) {
				array_push($data, array(
					'boletonovo_id' =>  intval($ret['nosso_numero']),
					'retorno_id' => $id,
					'data_pagamento' => date_format( date_create_from_format('dmy', $ret['dt_ocorrencia'] ), 'Y-m-d'),
					'valor_pagamento' => floatval( $ret['valor'] ) / 100
				));
				if ($this->Retorno->BoletoNovoRetorno->BoletoNovo->find('count', array(
					'conditions' => array(
						'BoletoNovo.id' => intval($ret['nosso_numero']),
						'BoletoNovo.situacao_id' => 1
					)
				)) == 1) {
					array_push($boletos, array(
						'id' => intval($ret['nosso_numero']),
						'situacao_id' => 2,
						'data_pagamento' => date_format( date_create_from_format('dmy', $ret['dt_ocorrencia'] ), 'Y-m-d')
					));
				}
			};
			$errors = false;
			if (count($boletos) > 0) {
				$this->Retorno->BoletoNovoRetorno->create();
				
				if (!$this->Retorno->BoletoNovoRetorno->saveMany($data)) $errors = true;
				if (!$this->Retorno->BoletoNovoRetorno->BoletoNovo->saveMany($boletos)) $errors = true;
			}
			return $errors;

		}
		
		public function upload() {
			
			$arquivo = $this->request->params['form']['file'];
			$path = APP.'retornos'.DS;
			

			$data = array(
				'arquivo' => $arquivo['name'],
				'data_envio' => date('Y-m-d'),
				'usuario_envio' => $this->Auth->user['id']
			);
			$this->Retorno->save($data);
			
			
			rename($arquivo['tmp_name'], $path.'retorno_'.$this->Retorno->id.'.ret');

			$error = $this->_baixa($this->Retorno->id);
			
			if (!$error) {
				$data = array(
					'msg' => 'Arquivo transferido com sucesso!',
					'error' => 0
				);
			} else {
				$data = array(
					'msg' => 'Erro na transferência ou no processamento do arquivo!',
					'error' => 1
				);
			}
			$this->set('data', $data);
			$this->set('_serialize', array('data'));
		}
		
	}