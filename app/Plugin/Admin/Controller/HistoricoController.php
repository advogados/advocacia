<?php
	class HistoricoController extends AdminAppController {
		
		public $uses = array('Api.Arquivo');
		public $components = array('RequestHandler');
				
		public function upload($tipo) {
			if ($tipo = 'adm') {
				$filename = 'historico_adm';
			} else {
				$filename = 'historico_pro';
			}
			
			$arquivo = $this->request->params['form']['file'];
			$ext = explode('.', $arquivo['name']);
			$ext = $ext[count($ext)-1];
			$path = APP.'historicos'.DS;
			
			$historico = array(
				'arquivo' => $arquivo['name'],
				'data_envio' => date('Y-m-d'),
				'usuario_envio' => $this->Auth->user['id']
			);
			
			$data = $this->Arquivo->save($historico);
			
			rename($arquivo['tmp_name'], $path.$filename.$this->Arquivo->id.'.'.$ext);

			$this->set('data', $data);
			$this->set('_serialize', array('data'));
		}
		
	}