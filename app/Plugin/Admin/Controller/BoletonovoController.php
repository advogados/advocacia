<?php
class BoletonovoController extends AdminAppController {

	public $uses = array('Api.BoletoNovo');
	
	public function view($id = null) {
		
		$this->layout = 'ajax';
		
		$boleto = $this->BoletoNovo->read(null, $id);
		
		$this->set('boleto', $boleto);
		
	}
}
