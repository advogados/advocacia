<?php
	class ArquivoController extends AdminAppController {
		
		public $uses = array('Api.Arquivo');
		public $components = array('RequestHandler');
		
		public function download($id) {
		
			$arquivo = $this->Arquivo->read(null, $id);
			
			$ext = explode('.', $arquivo['Arquivo']['arquivo']);
			$ext = $ext[count($ext)-1];
			$file = file_get_contents(APP.'historicos/historico_adm'.$arquivo['Arquivo']['id'].'.'.$ext);
			if ($ext == 'jpg') {
				header('Content-type: image/jpg');
			} else {
				header('Content-type: application/'.$ext);
				header('Content-Disposition: attachment; filename="' .$arquivo['Arquivo']['arquivo'].'"');
				header('Content-Transfer-Encoding: binary');
			}
			echo $file;
					
			$this->render(false);
		}

	}