<?php
	class UsuarioController extends AdminAppController {
		
		public $uses = array('Api.Usuario');
		
		public $components = array( 'RequestHandler' );
		
		public function beforeFilter() {
			$this->Auth->allow('login','token');
		}
		
		public function login() {
			$this->Usuario->setControllerAction('login');
			
			$this->layout = 'login';
			if ( $this->request->is('post') ) {
				
				if ($this->Auth->login()) {
					//$this->Session->setFlash('Login efetuado com sucesso!','Bootstrap.flash',array('type'=>'success'));
					$this->redirect('/');
				} else {
					//echo $this->Auth->password($this->data['Usuario']['password']);
					//$this->Session->setFlash('Usuário ou senha inválidos!','Bootstrap.flash',array('type'=>'danger'));
				}
				
			}
		}
		
		public function auth() {
			$this->layout = false;
			$data = CakeSession::read("Auth.User");
			$this->set('data', $data);
			$this->set('_serialize', array( 'data', 'pagination' ) );
			$this->render(false);
		}
		
		public function token() {
			echo $this->Auth->isLogged();
		}
		
		public function logout() {
			$this->Auth->logout();
			$this->redirect('/');
			$this->render(false);
		}
		
	}