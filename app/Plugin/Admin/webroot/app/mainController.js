sitesApp.cC({
	name: 'MainController',
	inject: ['$scope','ModelService','DataService', '$interval'],
	init: function() {
		this.Auth = this.ModelService.model.Auth;
		this.$.selectedTheme = this.DataService.Layout.theme;

		// session timer
		this.DataService.resetTimer = false;
		this.resetTimer();
		
		//this._bootswatch();
		this._loadUser();
	},
	watch: {
		'{object}selectedTheme':'changeTheme',
	},
	methods: {
		changeTheme: function(newv) {
			if (newv != this.DataService.Layout.theme) {
				$('#BWTheme').after('<style class="BWTheme">');
				$('.BWTheme').load(this.DataService.Layout.themePath+newv+'/bootstrap.min.css', function(){
					$('#BWTheme').remove();
					this.DataService.Layout.theme = newv;
				}.bind(this));
			}
		},
		_bootswatch: function() {
			
			this.ModelService.model.Bootswatch.get().then(function(data){
				this.$.BootswatchThemes = data.themes;
			}.bind(this));
		},
		_loadUser: function() {
			this.Auth.auth().then(function(data){
				//data.data.is_superuser = false;
				this.DataService.Usuario = data.data;
				this.$.nomeUsuario = data.data.nome;
				this.$.grupoUsuario = data.data.Grupo.id;
				this.$.isSuperUser = data.data.is_superuser;
				if (!this.useri) {
					this.useri = this.$interval(function(){this.userInterval();}.bind(this), 1000);
				}
			}.bind(this))
		},
		resetTimer: function() {
			this._loadUser();
			this.timer = new Date();
			this.timer.setMinutes(20);
			this.timer.setSeconds(01);
			this.DataService.resetTimer = false;
		},
		userInterval: function() {
			if (this.DataService.resetTimer) this.resetTimer();
			this.timer.setSeconds(this.timer.getSeconds()-1);
			min = this.timer.getMinutes();
			this.$.minutesToTimeout = parseInt(min);
			if (min < 10) min = '0'+min;
			sec = this.timer.getSeconds();
			if (sec < 10) sec = '0'+sec;
			
			if (min == '00' && sec == '00') {
				location.href = '/logout';
			}
			
			this.$.sessionTimeout = min+':'+sec;
		}
	}
});