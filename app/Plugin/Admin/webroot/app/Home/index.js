sitesApp.cC({
	name: 'homeCtrl',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		this._init();
	},
	data: function() {
		this.hoje = new Date();
		oldDate = this.hoje;
		this.amanha = nextday=new Date(oldDate.getFullYear(),oldDate.getMonth(),oldDate.getDate()+1);
		this.ontem = nextday=new Date(oldDate.getFullYear(),oldDate.getMonth(),oldDate.getDate()-1);
		this.BoletoNovo = this.ModelService.model.BoletoNovo;
		this.BoletoAntigo = this.ModelService.model.BoletoAntigo;
	},
	methods: {
		_init: function() {
			this.$.loadingOntem = true;
			this.$.loadingHoje = true;
			this.$.loadingAmanha = true;
			//Ontem
			this.BoletoNovo.get(
				{
					q: 'BoletoNovo.situacao_id.nq.4,BoletoNovo.data_vencimento.eq.'+this.ontem.getFullYear()+'-'+(this.ontem.getMonth()+1)+'-'+this.ontem.getDate(),
					populate: 'Sacado,SituacaoBoleto'
				}
			).then(function(data){
				this.$.BoletosOntemN = data.data;
				this.$.loadingOntem = false;
			}.bind(this));
			this.BoletoAntigo.get(
				{
					q: 'BoletoAntigo.situacao_id.nq.4,BoletoAntigo.data_vencimento.eq.'+this.ontem.getFullYear()+'-'+(this.ontem.getMonth()+1)+'-'+this.ontem.getDate(),
					populate: 'Sacado,SituacaoBoleto'
				}
			).then(function(data){
				this.$.BoletosOntemA = data.data;
				this.$.loadingOntem = false;
			}.bind(this));
			//Hoje
			this.BoletoNovo.get(
				{
					q: 'BoletoNovo.situacao_id.nq.4,BoletoNovo.data_vencimento.eq.'+this.hoje.getFullYear()+'-'+(this.hoje.getMonth()+1)+'-'+this.hoje.getDate(),
					populate: 'Sacado,SituacaoBoleto'
				}
			).then(function(data){
				this.$.BoletosHojeN = data.data;
				this.$.loadingHoje = false;
			}.bind(this));
			this.BoletoAntigo.get(
				{
					q: 'BoletoAntigo.situacao_id.nq.4,BoletoAntigo.data_vencimento.eq.'+this.hoje.getFullYear()+'-'+(this.hoje.getMonth()+1)+'-'+this.hoje.getDate(),
					populate: 'Sacado,SituacaoBoleto'
				}
			).then(function(data){
				this.$.BoletosHojeA = data.data;
				this.$.loadingHoje = false;
			}.bind(this));
			//Amanha
			this.BoletoNovo.get(
				{
					q: 'BoletoNovo.situacao_id.nq.4,BoletoNovo.data_vencimento.eq.'+this.amanha.getFullYear()+'-'+(this.amanha.getMonth()+1)+'-'+this.amanha.getDate(),
					populate: 'Sacado,SituacaoBoleto'
				}
			).then(function(data){
				this.$.BoletosAmanhaN = data.data;
				this.$.loadingAmanha = false;
			}.bind(this));
			this.BoletoAntigo.get(
				{
					q: 'BoletoAntigo.situacao_id.nq.4,BoletoAntigo.data_vencimento.eq.'+this.amanha.getFullYear()+'-'+(this.amanha.getMonth()+1)+'-'+this.amanha.getDate(),
					populate: 'Sacado,SituacaoBoleto'
				}
			).then(function(data){
				this.$.BoletosAmanhaA = data.data;
				this.$.loadingAmanha = false;
			}.bind(this));
		}
	}
});