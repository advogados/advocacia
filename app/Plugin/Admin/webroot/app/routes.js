sitesApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controller: 'homeCtrl', 
		templateUrl: '/admin/app/Home/index.html'
	})
	// Contas
	.when('/contas', {
		controller: 'ContaController', 
		templateUrl: '/admin/app/Conta/index.html'
	})
	.when('/contas/add', {
		controller: 'ContaFormController', 
		templateUrl: '/admin/app/Conta/form.html'
	})
	.when('/contas/edit/:id', {
		controller: 'ContaFormController', 
		templateUrl: '/admin/app/Conta/form.html'
	})	
	
	// Texto
	.when('/textos', {
		controller: 'TextoController', 
		templateUrl: '/admin/app/Texto/index.html'
	})
	.when('/textos/add', {
		controller: 'TextoFormController', 
		templateUrl: '/admin/app/Texto/form.html'
	})
	.when('/textos/edit/:id', {
		controller: 'TextoFormController', 
		templateUrl: '/admin/app/Texto/form.html'
	})
	
	// Sacados
	.when('/sacados', {
		controller: 'SacadoController', 
		templateUrl: '/admin/app/Sacado/index.html'
	})
	.when('/sacados/add', {
		controller: 'SacadoFormController', 
		templateUrl: '/admin/app/Sacado/form.html'
	})
	.when('/sacados/edit/:id', {
		controller: 'SacadoFormController', 
		templateUrl: '/admin/app/Sacado/form.html'
	})
	.when('/sacados/view/:id', {
		controller: 'SacadoViewController', 
		templateUrl: '/admin/app/Sacado/view.html'
	})
	.when('/sacados/procuracao/:id', {
		controller: 'DocumentoSacadosDocsController', 
		templateUrl: '/admin/app/Sacado/docs.html'
	})
	
	
	// Retornos
	.when('/retornos', {
		controller: 'RetornoController', 
		templateUrl: '/admin/app/Retorno/index.html'
	})
	.when('/retornos/add', {
		controller: 'RetornoFormController', 
		templateUrl: '/admin/app/Retorno/form.html'
	})
	.when('/retornos/edit/:id', {
		controller: 'RetornoFormController', 
		templateUrl: '/admin/app/Retorno/form.html'
	})

	// Documentos
	.when('/parcelamentos', {
		controller: 'DocumentoController', 
		templateUrl: '/admin/app/Parcelamento/index.html'
	})
	.when('/parcelamentos/add', {
		controller: 'DocumentoFormController', 
		templateUrl: '/admin/app/Parcelamento/form.html'
	})
	.when('/parcelamentos/edit/:id', {
		controller: 'DocumentoFormController', 
		templateUrl: '/admin/app/Parcelamento/form.html'
	})
	.when('/parcelamentos/view/:id', {
		controller: 'DocumentoViewController', 
		templateUrl: '/admin/app/Parcelamento/view.html'
	})
	.when('/parcelamentos/docs/:id', {
		controller: 'DocumentoDocsController', 
		templateUrl: '/admin/app/Parcelamento/docs.html'
	})
	.when('/parcelamentos/boletos/:id', {
		controller: 'DocumentoBoletosController', 
		templateUrl: '/admin/app/Parcelamento/boletos.html'
	})
	
	// Logs
	.when('/logs', {
		controller: 'LogController', 
		templateUrl: '/admin/app/Log/index.html'
	})
	
	// Usuários
	.when('/usuarios', {
		controller: 'UsuarioController', 
		templateUrl: '/admin/app/Usuario/index.html'
	})
	.when('/usuarios/add', {
		controller: 'UsuarioFormController', 
		templateUrl: '/admin/app/Usuario/form.html'
	})
	.when('/usuarios/edit/:id', {
		controller: 'UsuarioFormController', 
		templateUrl: '/admin/app/Usuario/form.html'
	})
	.when('/perfil', {
		controller: 'UsuarioFormController', 
		templateUrl: '/admin/app/Usuario/form.html'
	})
	
	// Relatórios
	.when('/relatorios/:tipo', {
		controller: 'RelatorioController', 
		templateUrl: '/admin/app/Relatorio/index.html'
	})
	
	.otherwise({
		templateUrl: '/admin/app/errors/404.html'
	});
	
});