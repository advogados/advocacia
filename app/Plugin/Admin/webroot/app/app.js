var sitesApp = angular.module('sitesApp', 
	['classy',
	'classy-on',
	'ngResource',
	'ngRoute',
	'ngAnimate',
	'ui.bootstrap',
	'ui.utils.masks',
	'idf.br-filters',
	'angularFileUpload',
	'ngWig',
	'frapontillo.bootstrap-switch',
	'mgcrea.bootstrap.affix',
	'ngAudio'
]);

var regexIso8601 = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\-(\d{4})$/;

function convertDateStringsToDates(input) {
	// Ignore things that aren't objects.
	if (typeof input !== "object") return input;
	
	for (var key in input) {
		if (!input.hasOwnProperty(key)) continue;
	
		var value = input[key];
		var match;
		// Check for string properties which look like dates.
		if (typeof value === "string" && (match = value.match(regexIso8601))) {
			var milliseconds = Date.parse(match[0]);
			if (!isNaN(milliseconds)) {
				input[key] = new Date(milliseconds);
			}
		} else if (typeof value === "object") {
			// Recurse into object
			convertDateStringsToDates(value);
		}
	}
}

sitesApp.config(["$httpProvider", function ($httpProvider) {
	$httpProvider.defaults.transformResponse.push(function(responseData){
		convertDateStringsToDates(responseData);
		return responseData;
	});
}]);

sitesApp.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
