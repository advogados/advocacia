sitesApp.cC({
	name: 'RelatorioController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		//if (!this.DataService.Usuario.is_superuser) {
		//	this.$location.path('/');
		//}
		this.$.Pesquisa = {
			nome_razaosocial: ''
		};
		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.BoletoNovo = this.ModelService.model.BoletoNovo;
		this.Relatorio = this.ModelService.model.Relatorio;
		
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.BoletoNovo.params.page = newv;
				this._load();
			}
		},
		pesqSacado: function() {
			this.$.pagination.page = 1;
			this._load();
		},
		related: function() {
		},
		_load: function() {
			this.$.loading = true;
			this.related();
			this.Relatorio.get().then(function(total){
				total_atrasado = total.data[0][0]['total'];
				this.$.total_atrasado = parseFloat(total_atrasado).toFixed(2);
			}.bind(this));
			this.BoletoNovo.get(
				{
					sort: '-data_vencimento',
					q: 'Sacado.nome_razaosocial.lk.'+this.$.Pesquisa.nome_razaosocial+',BoletoNovo.situacao_id.eq.1,BoletoNovo.data_vencimento.lt.'+new Date().toISOString(),
					populate: 'Sacado',
					page: this.BoletoNovo.params.page
				}
			).then(function(data){
				this.$.BoletosNovos = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		}
	}
});