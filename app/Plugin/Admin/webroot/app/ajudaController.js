sitesApp.cC({
	name: 'AjudaController',
	inject: ['$scope','ModelService','DataService'],
	init: function() {
		this.$.ajudaText = 'Texto da Ajuda Online.';
		this.$.ajuda_content_col = 12;
		this.$.ajudaEnabled = false;
	},
	methods: {
		toogleAjuda: function() {
			console.log(this.$.ajudaEnabled);
			if (this.$.ajudaEnabled) {
				this.$.ajudaEnabled  = false;
				this.$.ajuda_content_col = 12;
			} else {
				this.$.ajudaEnabled = true;
				this.$.ajuda_content_col = 9;
			}
		}
	}
});
