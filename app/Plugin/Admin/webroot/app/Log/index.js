sitesApp.cC({
	name: 'LogController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.$.loading = false;
		this.$.norecords = false;
		this.$.isView = false;
		this.$.Pesquisa = {
			usuario_id: this.DataService.Usuario.id
		};
		// Pra facilitar
		this.Log = this.ModelService.model.Log;
		this.Usuario = this.ModelService.model.Usuario;
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Log.params.page = newv;
				this._load();
			}
		},
		related: function() {
			this.Usuario.get({
				limit: 1000,
				sort: 'username'
			}).then(function(data){
				this.$.Usuarios = data.data;
			}.bind(this));
		},
		pesqUsuario: function() {
			this.$.pagination.page = 1;
			this._load();
		},
		_load: function() {
			this.$.loading = true;
			this.related();
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Log.get(
				{
					//q: 'Log.usuario_id.eq.'+this.$.Pesquisa.usuario_id,
					sort: '-data',
					page: this.$.pagination.page,
					populate: 'Usuario'
				}
			).then(function(data){
				this.$.Logs = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		view: function(item) {
			this.$.isView = true;
			this.$.dataView = item;
		},
		cancel: function() {
			delete(this.$.dataView);
			this.$.isView = false;
		}
	}
});