sitesApp.cC({
	name: 'TextoFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Texto = this.ModelService.model.Texto;

		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	
	methods: {
		
		_related: function(banco_id) {
			
		},
		load: function() {
			this.Texto.get(
				{
					id: this.$routeParams.id
				}
			)
			.then(function(data){
				this.$.Form = data.data.Texto;
				this._related();
			}.bind(this));
		},
		_add: function(){
			this.$.header = 'Novo Texto';
			this.$.Form = {};
			this._related();
		},
		_edit: function() {
			this.$.header = 'Editar Texto';
			this.load();
		},
		save: function() {
			if (this.$.TextoForm.$invalid) return false;
			this.Texto.save(this.$.Form);
			this.$location.path('/textos');
		},
		cancel: function() {
			this.$location.path('/textos');
		}
	}
});