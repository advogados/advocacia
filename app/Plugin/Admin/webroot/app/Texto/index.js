sitesApp.cC({
	name: 'TextoController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Texto = this.ModelService.model.Texto;
		
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Log.params.page = newv;
				this._load();
			}
		},
		related: function() {
		},
		_load: function() {
			this.$.loading = true;
			this.related();

			this.Texto.get(
				{
					sort: 'tipo',
					page: this.Texto.params.page
				}
			).then(function(data){
				this.$.Textos = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		add: function() {
			this.$location.path('/textos/add');
		},
		edit: function(item) {
			this.$location.path('/textos/edit/'+item.Texto.id);
		},
		del: function(item) {
			this.Texto.del(item.Texto.id).then(function(data){
				this._load();
			}.bind(this));
		}
	}
});