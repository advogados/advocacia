sitesApp.cC({
	name: 'UsuarioFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Usuario = this.ModelService.model.Usuario;
		this.Grupo = this.ModelService.model.Grupo;
		this.Estado = this.ModelService.model.Estado;
		this.Municipio = this.ModelService.model.Municipio;
		
		if (this.$location.path() == '/perfil') {
			this._perfil();
		} else if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
		'{object}Form.estado_id':'_changeEstado'
	},
	methods: {
		_related: function() {
			this.Grupo.get({
				limit: 100
			})
			.then(function(data){
				this.$.Grupos = data.data;
			}.bind(this));
		},
		_changeEstado: function(newv, oldv) {
			if (newv != oldv) {
				if (newv != null) {
					this.Municipio.get(
						{
							limit: 100,
							sort: 'nome',
							q: 'Municipio.estado_id.eq.'+newv
						}
					)
					.then(function(data){
						this.$.Municipios = data.data;
					}.bind(this));
				} else {
					delete(this.$.Municipios);
				}
			}
		},
		_add: function(){
			this.$.header = 'Novo Usuário';
			this._related();
		},
		_edit: function() {
			this.$.header = 'Editar Usuário';
			this._related();
			this.Usuario.get({id: this.$routeParams.id})
			.then(function(data){
				this.$.Form = data.data.Usuario;
			}.bind(this));
		},
		_perfil: function() {
			this.$.header = 'Perfil';
			this._related();
			this.Usuario.get(
				{
					id: this.DataService.Usuario.id
				}
			).then(function(data){
				this.$.Form = data.data.Usuario;
			}.bind(this));
		},
		save: function() {
			this.Usuario.save(this.$.Form);
			this.$location.path('/usuarios');
		},
		cancel: function() {
			this.$location.path('/usuarios');
		}
	}
});