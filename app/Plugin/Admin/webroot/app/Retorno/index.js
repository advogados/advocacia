sitesApp.cC({
	name: 'RetornoController',
	inject: ['$scope','ModelService','DataService','$location','$timeout','FileUploader'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.$.loading = false;
		this.$.norecords = false;
		
		// Pra facilitar
		this.Retorno = this.ModelService.model.Retorno;
		this.RetornoBaixa = this.ModelService.model.RetornoBaixa;
		
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		this.$.uploader = new this.FileUploader({
			url: '/admin/retorno/upload.json',
			autoUpload: true,
			removeAfterUpload: true,
			onSuccessItem:	function() {
				this.$.loading = true;
				this.$.Retornos = [];
				this.$timeout(function(){
					this._load();
				}.bind(this), 1000)
			}.bind(this)
		});
		this.$.uploader.filters.push({
			name: 'RetornoFilter',
			fn: function(item /*{File|FileLikeObject}*/, options) {
				var type = item.type;
				return type == '';
			}
		});
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Retorno.params.page = newv;
				this._load();
			}
		},
		related: function() {
			
		},
		
		_load: function() {
			this.$.loading = true;
			this.$.norecords = false;
			this.related();
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Retorno.get(
				{
					sort: 'data_envio DESC',
					page: this.$.pagination.page,
					populate: 'BoletoNovoRetorno.BoletoNovo.Sacado,BoletoNovoRetorno.BoletoNovo.SituacaoBoleto'
				}
			).then(function(data){
				this.$.Retornos = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		del: function(item) {
			if (confirm('Tem Certeza?')) {
				this.Retorno.del(item.Retorno.id).then(function(data){
					this._load();
				}.bind(this));
			}
		},
		_darBaixa: function(id) {
			this.RetornoBaixa.get(
				{
					id: id
				}
			).then(function(data){
				console.log(data);
			}.bind(this));
		}
	}
});