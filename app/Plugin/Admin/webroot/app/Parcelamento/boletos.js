sitesApp.cC({
	name: 'DocumentoBoletosController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Documento = this.ModelService.model.Documento;
		this._boletos();
	},
	watch: {
	},
	methods: {
		_related: function() {
			
		},
		
		
		load: function() {
			this.Documento.get(
				{
					id: this.$routeParams.id,
					populate: 'BoletoNovo'
				}
			)
			.then(function(data){
				this.$.Documento = data.data;
			}.bind(this));
		},
		_boletos: function() {
			this.$.header = 'Boletos do Documento';
			this.load();
		},
		
		cancel: function() {
			this.$location.path('/documentos');
		}
	}
});