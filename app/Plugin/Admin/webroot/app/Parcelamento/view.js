sitesApp.cC({
	name: 'DocumentoViewController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Documento = this.ModelService.model.Sacado;
		this._view();
	},
	watch: {
	},
	methods: {
		_related: function() {
			
		},
		
		
		load: function() {
			this.Documento.get({id: this.$routeParams.id})
			.then(function(data){
				Documento = data.data.Sacado;
				this.$.Form = Documento;
			}.bind(this));
		},
		_view: function() {
			this.$.header = 'Ver Documento';
			this.load();
		},
		
		cancel: function() {
			this.$location.path('/documentos');
		}
	}
});