sitesApp.cC({
	name: 'DocumentoDocsController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams','dateFilter'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Documento = this.ModelService.model.Documento;
		this.Texto = this.ModelService.model.Texto;
		this._docs();
	},
	data: {
		textoVars: {
			'outorgante_nome':'Outorgante.nome_razaosocial',
			'outorgante_endereco':'Outorgante.endereco',
			'outorgante_cidade':'Outorgante.cidade',
			'outorgante_uf':'Outorgante.uf',
			'outorgante_telefone':'Outorgante.telefone',
			
			'outorgante_nacionalidade':'Outorgante.nacionalidade',
			'outorgante_estadocivil_descricao':'Outorgante.EstadoCivil.descricao',
			'outorgante_profissao':'Outorgante.profissao',
			'outorgante_cpfcnpj':'Outorgante.cpf_cnpj',
			'outorgante_docnumero':'Outorgante.docnumero',
			'outorgante_docorgao':'Outorgante.docorgao',
			
			'representante_nome':'Representante.nome_razaosocial',
			'representante_endereco':'Representante.endereco',
			'representante_cidade':'Representante.cidade',
			'representante_uf':'Representante.uf',
			'representante_telefone':'Representante.telefone',

			'representante_nacionalidade':'Representante.nacionalidade',
			'representante_estadocivil_descricao':'Representante.EstadoCivil.descricao',
			'representante_profissao':'Representante.profissao',
			'representante_cpfcnpj':'Representante.cpf_cnpj',
			'representante_docnumero':'Representante.docnumero',
			'representante_docorgao':'Representante.docorgao',
			
			'documento_valor':'Documento.valor',
			'documento_data_hoje':'Documento.data|date',
			'documento_data':'Documento.data|date',
			
			'dia_doc_extenso':'Documento.data|day|extenso',
			'mes_doc_extenso':'Documento.data|month|extenso',
			'ano_doc_extenso':'Documento.data|year|extenso',
			
			'documento_valor_extenso':'Documento.valor|extenso',
			'documento_parcelas':'Documento.parcelas',
			'documento_parcelas_restantes':'Documento.parcelas_restantes',
			'documento_parcelas_valor':'Documento.valor_parcelas',
			'documento_temrepresentante':'Documento.temrepresentante'
		}
	},
	watch: {
		'{object}selectedTipo': '_textoReplace',
		'{object}originalText': '_changeText'
	},
	methods: {
		_related: function() {
			this.Texto.get().then(function(records){
				this.$.Textos = records.data;
			}.bind(this));
		},
		
		_changeText: function(newv, oldv) {
			if (newv == oldv) return;
			this.$.replacedText = newv;
			this._ifReplace();
			this._realReplace();
		},
		
		_textoReplace: function(newv, oldv) {
			if (newv == oldv) return;
			if (newv == null) return;
			if (newv == '') return;
			this.$.replacedText = this.$.Textos[newv].Texto.texto;
			this.$.originalText = this.$.Textos[newv].Texto.texto;
			this._realReplace();
		},
		
		_ifReplace: function() {
			angular.forEach(this.textoVars, function(value, index, arr){
				var reg = new RegExp('\\[if '+index+'\\]');
				
				findIf = this.$.replacedText.search(reg);
								
				if (findIf > -1) {
					var reg = new RegExp('\\[elseif '+index+'\\]');
					findElse = this.$.replacedText.search(reg);
					var reg = new RegExp('\\[endif '+index+'\\]');
					findEnd = this.$.replacedText.search(reg);
					
					beforeText = this.$.replacedText.slice(0, findIf);
					
					if (this._evalValue(value) || ( this._evalValue(value) != '' && this._evalValue(value) != null )) {
						middleText = this.$.replacedText.slice(findIf+('[if '+index+']').length, findElse);
					} else {
						middleText = this.$.replacedText.slice(findElse+('[elseif '+index+']').length, findEnd);
					}
					
					afterText = this.$.replacedText.slice(findEnd+('[endif '+index+']').length);
					
					this.$.replacedText = beforeText + middleText + afterText;
				}
			}.bind(this));
			
		},
		
		_realReplace: function() {
			angular.forEach(this.textoVars, function(value, index, arr){
				var re = new RegExp('\\['+index+'\\]', "g");
				this.$.replacedText = this.$.replacedText.replace(re, this._evalValue(value));
			}.bind(this));
			
		},
		
		_evalValue: function(value) {
			value = value.split('|');
			values = value[0].split('.');

			if (value.length == 1) {
				return this.$.Documento[values[0]][values[1]];
			} else {
				res = this.$.Documento[values[0]][values[1]];
				
				i = 1;
				while(undefined != value[i]) {
					
					if (value[i] == 'date') res =  this.dateFilter( res, 'dd/MM/yyyy' );
					if (value[i] == 'day') res = this.dateFilter( res, 'dd' );
					if (value[i] == 'month') res = this.dateFilter( res, 'MM' );
					if (value[i] == 'year') res = this.dateFilter( res, 'yyyy' );
					
					if (value[i] == 'extenso') {
						res = parseFloat(res).toString();
						res = res.extenso();
					}
					i++;
				}
				
				return res;
			}
		},
		
		load: function() {
			this.Documento.get(
				{
					id: this.$routeParams.id,
					populate: 'Outorgante.EstadoCivil,Representante.EstadoCivil'
				}
			).then(function(records){
				records.data.Documento.valor = parseFloat(records.data.Documento.valor).toFixed(2);
				records.data.Documento.valor_parcelas = parseFloat( records.data.Documento.valor / records.data.Documento.parcelas ).toFixed(2);
				records.data.Documento.parcelas_restantes = parseInt(records.data.Documento.parcelas) - 1;
				this.$.Documento = records.data;
			}.bind(this));
		},
		_docs: function() {
			this.$.header = 'Documentos do Parcelamento';
			this._related();
			this.load();
		},
		print: function() {
			window.print();
		},
		
		cancel: function() {
			this.$location.path('/documentos');
		}
	}
});
