sitesApp.cC({
	name: 'DocumentoController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.$.loading = false;
		this.$.norecords = false;
		
		this.Documento = this.ModelService.model.Documento;
		
		this.$.Pesquisa = {
			nome_razaosocial: ''
		};
		
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Documento.params.page = newv;
				this._load();
			}
		},
		related: function() {
		
		},
		pesqParcelamento: function() {
			this.$.pagination.page = 1;
			this._load();
		},
		
		_load: function() {
			this.$.loading = true;
			this.related();
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Documento.get(
				{
					q: 'Outorgante.nome_razaosocial.lk.'+this.$.Pesquisa.nome_razaosocial,
					sort: '-data',
					page: this.Documento.params.page,
					populate: 'Outorgante,Representante,BoletoNovo'
				}
			).then(function(data){
				idx = 0;
				data.data.forEach(function(item){
					pagos = 0;
					item.BoletoNovo.forEach(function(boleto){
						if (boleto.situacao_id == 4) {
							pagos++;
						}
					}.bind(this));
					data.data[idx].Documento.parcelas_pagas = pagos;
					//console.log(data.data[idx].Documento.parcelas_pagas = pagos);
					idx++;
				}.bind(this));
				this.$.Documentos = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		add: function() {
			this.$location.path('/parcelamentos/add');
		},
		edit: function(item) {
			this.$location.path('/parcelamentos/edit/'+item.Documento.id);
		},
		view: function(item) {
			this.$location.path('/parcelamentos/view/'+item.Documento.id);
		},
		boletos: function(item) {
			this.$location.path('/sacados/view/'+item.Outorgante.id);
		}
	}
});