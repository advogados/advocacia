sitesApp.cC({
	name: 'DocumentoFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Documento = this.ModelService.model.Documento;
		this.BoletoNovo = this.ModelService.model.BoletoNovo;
		this.Sacado = this.ModelService.model.Sacado;
		
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
		'{object}sacado_nome':'_searchSacado',
		'{object}representante_nome':'_searchRepresentante'
	},
	methods: {
		_related: function() {
			/*this.Estado.get()
			.then(function(data){
				this.$.Estados = data.data;
			}.bind(this));
			*/
		},
		_searchSacado: function(newv, oldv) {
			if (newv == oldv) return false;
			if (newv.length < 3) return false;
			this.Sacado.get(
				{
					q: 'Sacado.nome_razaosocial.lk.'+newv
				}
			).then(function(data){
				this.$.Sacados = data.data;
			}.bind(this));
		},
		_searchRepresentante: function(newv, oldv) {
			if (newv == oldv) return false;
			if (newv.length < 3) return false;
			this.Sacado.get(
				{
					q: 'Sacado.nome_razaosocial.lk.'+newv
				}
			).then(function(data){
				this.$.Representantes = data.data;
			}.bind(this));
		},
		calcParcelas: function() {
			if ((this.$.Form.valor) && (this.$.Form.parcelas)) {
				this.$.valor_parcelas = (this.$.Form.valor / this.$.Form.parcelas).toFixed(2);
			}
		},
		_add: function(){
			this.$.header = 'Novo Parcelamento';
			this.$.Form = {};
			this._related();
		},
		load: function() {
			this.Documento.get(
				{
					id: this.$routeParams.id,
					populate: 'Outorgante,Representante,BoletoNovo'
				}
			).then(function(data){
				Documento = data.data.Documento;
				Documento.valor = parseFloat(Documento.valor);
				Documento.parcelas = parseInt(Documento.parcelas);
				this.$.Form = Documento;
				this.$.sacado_nome = data.data.Outorgante.nome_razaosocial;
				this.$.representante_nome = data.data.Representante.nome_razaosocial;
				this.calcParcelas();
			}.bind(this));
		},
		_edit: function() {
			this.$.header = 'Editar Parcelamento';
			this._related();
			this.load();
		},
		save: function() {
			this.Documento.save(this.$.Form).then(function(data){
				saved = data.data.Documento;
				if (this.$.Form.id == undefined) {
					for (b=0;b<saved.parcelas;b++) {
						data_vencimento = new Date();
						data_vencimento.setTime(this.$.Form.data.getTime());
						data_vencimento.setMonth(this.$.Form.data.getMonth()+(1*b));
						boleto = {
							valor: (saved.valor/saved.parcelas).toFixed(2),
							data_vencimento: data_vencimento.toISOString(),
							sacado_id: this.$.Form.outorgante_id,
							situacao_id: 1,
							documento_id: saved.id
						};
						this.BoletoNovo.save(boleto);
					}
				}
			}.bind(this));
			this.$location.path('/parcelamentos');
		},
		cancel: function() {
			this.$location.path('/parcelamentos');
		}
	}
});