sitesApp.cC({
	name: 'ContaController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.$.loading = false;
		this.$.norecords = false;
		// Pra facilitar
		this.Conta = this.ModelService.model.Conta;
		
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Log.params.page = newv;
				this._load();
			}
		},
		related: function() {
		},
		_load: function() {
			this.$.loading = true;
			this.related();

			this.Conta.get(
				{
					sort: 'agencia_id',
					page: this.$.pagination.page,
					populate: 'Agencia.Banco'
				}
			).then(function(data){
				this.$.Contas = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		add: function() {
			this.$location.path('/contas/add');
		},
		edit: function(item) {
			this.$location.path('/contas/edit/'+item.Conta.id);
		},
		del: function(item) {
			if (!confirm('Tem Certeza?')) return false;
			this.Conta.del(item.Conta.id).then(function(data){
				this._load();
			}.bind(this));
		}
	}
});