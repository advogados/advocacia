sitesApp.cC({
	name: 'ContaFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Conta = this.ModelService.model.Conta;
		this.Banco = this.ModelService.model.Banco;
		this.Agencia = this.ModelService.model.Agencia;
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
		'{object}Form.banco_id':'_loadAgencias'
	},
	methods: {
		_loadAgencias: function(oldv, newv) {
			if (oldv == newv) return;
			this.Agencia.get(
				{
					q: 'Agencia.banco_id.eq.'+this.$.Form.banco_id
				}
			).then(function(data){
				this.$.Agencias = data.data;
			}.bind(this));
		},
		_related: function(banco_id) {
			this.Banco.get().then(function(data){
				this.$.Bancos = data.data;
			}.bind(this));
			this.Agencia.get(
				{
					q: 'Agencia.banco_id.eq.'+banco_id
				}
			).then(function(data){
				this.$.Agencias = data.data;
			}.bind(this));
		},
		load: function() {
			this.Conta.get(
				{
					id: this.$routeParams.id,
					populate: 'Agencia'
				}
			)
			.then(function(data){
				Conta = data.data.Conta;
				Conta.numero = parseInt(Conta.numero); // Inteiro
				Conta.saldo = parseFloat(Conta.saldo); // Decimal
				Conta.banco_id = data.data.Agencia.banco_id;
				this.$.Form = Conta;
				this._related(data.data.Agencia.banco_id);
			}.bind(this));
		},
		_add: function(){
			this.$.header = 'Nova Conta';
			this.$.Form = {
				banco_id: 0
			};
			this._related();
		},
		_edit: function() {
			this.$.header = 'Editar Conta';
			this.load();
		},
		save: function() {
			if (this.$.ContaForm.$invalid) return false;
			this.Conta.save(this.$.Form);
			this.$location.path('/contas');
		},
		cancel: function() {
			this.$location.path('/contas');
		}
	}
});