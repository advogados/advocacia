sitesApp.cC({
	name: 'SacadoViewController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams','FileUploader'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Sacado = this.ModelService.model.Sacado;
		this.Arquivo = this.ModelService.model.Arquivo;
		this.HistoricoAdm = this.ModelService.model.HistoricoAdm;
		this.HistoricoPro = this.ModelService.model.HistoricoPro;
		this.BoletoNovo = this.ModelService.model.BoletoNovo;
		
		this.$.isAddHistoricoAdm = false;
		this.$.isAddHistoricoPro = false;
		
		this.$.uploaderadm = new this.FileUploader({
			url: '/admin/historico/upload/adm.json',
			autoUpload: true,
			removeAfterUpload: false,
			onSuccessItem: function(item, res){
				this.$.HistoricoAdmForm.arquivo_id = res.data.Arquivo.id;
			}.bind(this)
		});
		this.$.uploaderpro = new this.FileUploader({
			url: '/admin/historico/upload/pro.json',
			autoUpload: true,
			removeAfterUpload: false,
			onSuccessItem: function(item, res){
				this.$.HistoricoProForm.arquivo_id = res.data.Arquivo.id;
			}.bind(this)
		});
		
		this._view();
	},
	watch: {
	},
	methods: {
		_newBolNovo: function() {
			this.$.formBolNovo = {
				situacao_id: 1,
				sacado_id: this.$.data.Sacado.id
			};
		},
		saveBolNovo: function() {
			this.BoletoNovo.save(this.$.formBolNovo).then(function(data){
				this._view();
			}.bind(this));
		},
		delBolNovo: function(item) {
			if(confirm('Tem Certeza?')) {
				this.BoletoNovo.del(item.id)
				.then(function(data) {
					this._view();
				}.bind(this))
			}
		},
		_related: function() {
			/*this.Estado.get()
			.then(function(data){
				this.$.Estados = data.data;
			}.bind(this));
			*/
		},
		delAdm: function(item) {
			if (confirm('Tem Certeza?')) {
				this.HistoricoAdm.del(item.id)
					.then(function(data){
						this._view();
					}.bind(this));
			}
		},
		delPro: function(item) {
			if (confirm('Tem Certeza?')) {
				this.HistoricoPro.del(item.id)
					.then(function(data){
						this._view();
					}.bind(this));
			}
		},
		_view: function() {
			this.$.is_superuser = this.DataService.Usuario.is_superuser;
			this.$.user_id = this.DataService.Usuario.id;
			this.$.loading = true;
			this.$.today = new Date();
			this.Sacado.get(
				{
					id: this.$routeParams.id,
					populate: 
						'BoletoAntigo.SituacaoBoleto,'+
						'BoletoNovo.SituacaoBoleto,'+
						'HistoricoAdm.Arquivo,'+
						'HistoricoAdm.Usuario,'+
						'HistoricoPro.Arquivo,'+
						'HistoricoPro.Usuario,'+
						'BoletoNovo.Documento,' +
						'BoletoNovo.UserExcl'
					
				}
			).then(function(data){
				this.$.data = data.data;
				this._newBolNovo();
				this.$.loading = false;
			}.bind(this));
		},
		newHistoricoAdm: function() {
			this.$.isAddHistoricoAdm = true;
			this.$.HistoricoAdmForm = {
				sacado_id: this.$routeParams.id,
				data: new Date(),
				user_id: this.DataService.Usuario.id
			};
		},
		newHistoricoAdmCancel: function() {
			delete(this.$.HistoricoAdmForm);
			this.$.isAddHistoricoAdm = false;
		},
		newHistoricoAdmSave: function() {
			
			this.HistoricoAdm.save(this.$.HistoricoAdmForm).then(function(data){
				if (this.$.HistoricoAdmForm.arquivo_id != undefined) {
					this.Arquivo.get(
						{
							id: this.$.HistoricoAdmForm.arquivo_id
						}
					).then(function(arq){
						arquivo = arq.data.Arquivo;
						arquivo.historicoadm_id = data.data.HistoricoAdm.id;
						this.Arquivo.save(this.$.HistoricoAdmForm.arquivo_id,arquivo);
	
						delete(this.$.data);
						delete(this.$.HistoricoAdmForm);
						this.$.isAddHistoricoAdm = false;
						this._view();
					}.bind(this));
				} else {
					this.$.isAddHistoricoAdm = false;
					this._view();
				}
			}.bind(this));
		},
		
		newHistoricoPro: function() {
			this.$.isAddHistoricoPro = true;
			this.$.HistoricoProForm = {
				sacado_id: this.$routeParams.id,
				data: new Date(),
				user_id: this.DataService.Usuario.id
			};
		},
		newHistoricoProCancel: function() {
			delete(this.$.HistoricoProForm);
			this.$.isAddHistoricoPro = false;
		},
		newHistoricoProSave: function() {
			this.HistoricoPro.save(this.$.HistoricoProForm).then(function(data){
				if (this.$.HistoricoProForm.arquivo_id != undefined) {
					this.Arquivo.get(
						{
							id: this.$.HistoricoProForm.arquivo_id
						}
					).then(function(arq){
						arquivo = arq.data.Arquivo;
						arquivo.historicopro_id = data.data.HistoricoPro.id;
						this.Arquivo.save(this.$.HistoricoProForm.arquivo_id,arquivo);
						delete(this.$.data);
						delete(this.$.HistoricoProForm);
						this.$.isAddHistoricoPro = false;
						this._view();
					}.bind(this));
				} else {
					this.$.isAddHistoricoPro = false;
					this._view();
				}
			}.bind(this));
		}
	}
});
