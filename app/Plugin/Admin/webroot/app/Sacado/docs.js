sitesApp.cC({
	name: 'DocumentoSacadosDocsController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams','dateFilter'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.Outorgante = this.ModelService.model.Sacado;
		this.Texto = this.ModelService.model.Texto;
		this._docs();
	},
	data: {
		textoVars: {
			'outorgante_nome':'Sacado.nome_razaosocial',
			'outorgante_endereco':'Sacado.endereco',
			'outorgante_cidade':'Sacado.cidade',
			'outorgante_uf':'Sacado.uf',
			'outorgante_telefone':'Sacado.telefone',
			
			'outorgante_nacionalidade':'Sacado.nacionalidade',
			'outorgante_estadocivil_descricao':'Sacado.EstadoCivil.descricao',
			'outorgante_profissao':'Sacado.profissao',
			'outorgante_cpfcnpj':'Sacado.cpf_cnpj',
			'outorgante_docnumero':'Sacado.docnumero',
			'outorgante_docorgao':'Sacado.docorgao',
						
			//'documento_valor':'Documento.valor',
			//'documento_data_hoje':'Documento.data|date',
			//'documento_data':'Documento.data|date',
			
			//'dia_doc_extenso':'Documento.data|day|extenso',
			//'mes_doc_extenso':'Documento.data|month|extenso',
			//'ano_doc_extenso':'Documento.data|year|extenso',
			
			//'documento_valor_extenso':'Documento.valor|extenso',
			//'documento_parcelas':'Documento.parcelas',
			//'documento_parcelas_restantes':'Documento.parcelas_restantes',
			//'documento_parcelas_valor':'Documento.valor_parcelas',
			//'documento_temrepresentante':'Documento.temrepresentante'
		}
	},
	watch: {
		'{object}selectedTipo': '_textoReplace',
		'{object}originalText': '_changeText'
	},
	methods: {
		_related: function() {
			this.Texto.get().then(function(records){
				this.$.Textos = records.data;
				this._textoReplace(1, null);
			}.bind(this));
		},
		
		_changeText: function(newv, oldv) {
			if (newv == oldv) return;
			this.$.replacedText = newv;
			this._ifReplace();
			this._realReplace();
		},
		
		_textoReplace: function(newv, oldv) {
			if (newv == oldv) return;
			if (newv == null) return;
			if (newv == '') return;
			this.$.replacedText = this.$.Textos[newv].Texto.texto;
			this.$.originalText = this.$.Textos[newv].Texto.texto;
			this._realReplace();
		},
		
		_ifReplace: function() {
			angular.forEach(this.textoVars, function(value, index, arr){
				var reg = new RegExp('\\[if '+index+'\\]');
				
				findIf = this.$.replacedText.search(reg);
								
				if (findIf > -1) {
					var reg = new RegExp('\\[elseif '+index+'\\]');
					findElse = this.$.replacedText.search(reg);
					var reg = new RegExp('\\[endif '+index+'\\]');
					findEnd = this.$.replacedText.search(reg);
					
					beforeText = this.$.replacedText.slice(0, findIf);
					
					if (this._evalValue(value) || ( this._evalValue(value) != '' && this._evalValue(value) != null )) {
						middleText = this.$.replacedText.slice(findIf+('[if '+index+']').length, findElse);
					} else {
						middleText = this.$.replacedText.slice(findElse+('[elseif '+index+']').length, findEnd);
					}
					
					afterText = this.$.replacedText.slice(findEnd+('[endif '+index+']').length);
					
					this.$.replacedText = beforeText + middleText + afterText;
				}
			}.bind(this));
			
		},
		
		_realReplace: function() {
			angular.forEach(this.textoVars, function(value, index, arr){
				var re = new RegExp('\\['+index+'\\]', "g");
				this.$.replacedText = this.$.replacedText.replace(re, this._evalValue(value));
			}.bind(this));
			
		},
		
		_evalValue: function(value) {
			console.log(value);
			value = value.split('|');
			values = value[0].split('.');
			return this.$.Documento[values[0]][values[1]];
		},
		
		load: function() {
			this.Outorgante.get(
				{
					id: this.$routeParams.id,
					populate: 'EstadoCivil'
				}
			).then(function(records){
				this.$.Documento = records.data;
				console.log(records.data);
			}.bind(this));
		},
		_docs: function() {
			this.$.header = 'Documentos do Parcelamento';
			this._related();
			this.load();
		},
		print: function() {
			window.print();
		},
		
		cancel: function() {
			this.$location.path('/documentos');
		}
	}
});