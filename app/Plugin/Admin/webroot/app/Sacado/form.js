sitesApp.cC({
	name: 'SacadoFormController',
	inject: ['$scope','ModelService','DataService','$location','$routeParams'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;
		
		this.Sacado = this.ModelService.model.Sacado;
		this.EstadoCivil = this.ModelService.model.EstadoCivil;
		this.BuscaCEP = this.ModelService.model.BuscaCEP;
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
	},
	methods: {
		_related: function() {
			this.EstadoCivil.get()
			.then(function(data){
				this.$.EstadosCivis = data.data;
			}.bind(this));
			
		},
		pesqEndereco: function() {
			this.BuscaCEP.get({
				cep: this.$.Form.cep
			}).then(function(data){
				if (data.status == 0) {
					alert('O CEP não foi encontrado!');
				} else {
					this.$.Form.endereco = data.address;
					this.$.Form.cidade = data.city;
					this.$.Form.bairro = data.district;
					this.$.Form.uf = data.state;
				}
			}.bind(this));
		},
		_add: function(){
			this.$.header = 'Novo Sacado';
			this._related();
		},
		load: function() {
			this.Sacado.get({id: this.$routeParams.id})
			.then(function(data){
				Sacado = data.data.Sacado;
				this.$.Form = Sacado;
				if (this.$.Form.ddd || this.$.Form.telefone) {
					this.$.Form.ddd_telefone = this.$.Form.ddd.trim()+this.$.Form.telefone.trim();
				} else {
					this.$.Form.ddd_telefone = '';
				}
				if (this.$.Form.ddd || this.$.Form.telefone2) {
					this.$.Form.ddd_telefone2 = this.$.Form.ddd.trim()+this.$.Form.telefone2.trim();
				} else {
					this.$.Form.ddd_telefone2 = '';
				}
			}.bind(this));
		},
		_edit: function() {
			this.$.header = 'Editar Sacado';
			this._related();
			this.load();
		},
		save: function() {
			if (this.$.SacadoForm.$invalid) return false;
			if (this.$.Form.ddd_telefone) {
				this.$.Form.ddd = this.$.Form.ddd_telefone.substr(0, 2);
				this.$.Form.telefone = this.$.Form.ddd_telefone.substr(2);
			}
			if (this.$.Form.ddd_telefone2) {
				this.$.Form.ddd = this.$.Form.ddd_telefone2.substr(0, 2);
				this.$.Form.telefone2 = this.$.Form.ddd_telefone2.substr(2);
			}
			this.$.Form.nome_razaosocial = this.$.Form.nome_razaosocial.toUpperCase().trim();
			delete(this.$.Form.ddd_telefone);
			this.Sacado.save(this.$.Form);
			this.$location.path('/sacados');
		},
		cancel: function() {
			this.$location.path('/sacados');
		}
	}
});