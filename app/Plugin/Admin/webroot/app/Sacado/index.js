sitesApp.cC({
	name: 'SacadoController',
	inject: ['$scope','ModelService','DataService','$location'],
	init: function() {
		// reset user session timeout
		this.DataService.resetTimer = true;

		this.$.loading = false;
		this.$.norecords = false;
		this.$.Pesquisa = {
			nome_razaosocial: ''
		};
		// Pra facilitar
		this.Sacado = this.ModelService.model.Sacado;
		// Paginação
		this.$.pagination = {
			page: 1,
			limit: 10,
			count: 0,
			pageCount: 1
		};
		// Método ao iniciar
		this._load();
	},
	watch: {
		// Observando mudança de página
		'{object}pagination.page':'_reload'
	},
	methods: {
		pesquisaTyping: function(event) {
			if(event.keyCode == 13) {
				this.pesqSacado();	
			}	
		},
		_reload: function(newv, oldv) {
			// Carrega nova página apenas se o valor novo for diferente do valor antigo
			if (newv != oldv) {
				this.Sacado.params.page = newv;
				this._load();
			}
		},
		pesqSacado: function() {
			this.$.pagination.page = 1;
			this._load();
		},
		_load: function() {
			this.$.loading = true;
			// Ler as noticias do banco através do ModelService
			// Todo: Tem uma forma melhor, sem usar o .then ?
			this.Sacado.get(
				{
					q: 'Sacado.nome_razaosocial.lk.'+this.$.Pesquisa.nome_razaosocial,
					sort: 'nome_razaosocial',
					page: this.$.pagination.page,
					populate: 'BoletoAntigo,BoletoNovo'
				}
			).then(function(data){
				this.$.Sacados = data.data;
				this.$.pagination = data.pagination;
				this.$.loading = false;
				if (data.data.length == 0) {
					this.$.norecords = true;
				}
			}.bind(this));
		},
		del: function() {
			if (!confirm('Tem Certeza?')) return false;
		},
		add: function() {
			this.$location.path('sacados/add');
		},
		edit: function(item) {
			this.$location.path('sacados/edit/'+item.Sacado.id);
		},
		view: function(item) {
			this.$location.path('sacados/view/'+item.Sacado.id);
		},
		procuracao: function(item) {
			this.$location.path('sacados/procuracao/'+item.Sacado.id);
		}
	}
});