sitesApp.service('ModelService', ['$resource','DataService', function($resource, DataService) {
	this.model = {
		
		Relatorio: {
			res: $resource('/api/boletosnovos/total_atrasado.json'),
			get: function(params) {
				return this.model.Relatorio.res.get(params).$promise;
			}.bind(this)
		},
		Banco: {
			res: $resource('/api/bancos/:id.json'),
			params: {
				sort: 'nome'
			},
			page: 1,
			get: function(params) {
				return this.model.Banco.res.get((params)?params:this.model.Banco.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.Banco.res.save(params).$promise;
			}.bind(this)
		},
		Arquivo: {
			res: $resource('/api/arquivos/:id.json'),
			page: 1,
			get: function(params) {
				return this.model.Arquivo.res.get((params)?params:this.model.Arquivo.params).$promise;
			}.bind(this),
			save: function(id,params) {
				return this.model.Arquivo.res.save({id:id}, params).$promise;
			}.bind(this)
		},
		HistoricoAdm: {
			res: $resource('/api/historicoadm/:id.json'),
			params: {
				sort: 'nome'
			},
			page: 1,
			get: function(params) {
				return this.model.HistoricoAdm.res.get((params)?params:this.model.HistoricoAdm.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.HistoricoAdm.res.save(params).$promise;
			}.bind(this),
			del: function(id) {
				return this.model.HistoricoAdm.res.delete({id:id}).$promise;
			}.bind(this)
		},
		HistoricoPro: {
			res: $resource('/api/historicopro/:id.json'),
			params: {
				sort: 'nome'
			},
			page: 1,
			get: function(params) {
				return this.model.HistoricoPro.res.get((params)?params:this.model.HistoricoPro.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.HistoricoPro.res.save(params).$promise;
			}.bind(this),
			del: function(id) {
				return this.model.HistoricoPro.res.delete({id:id}).$promise;
			}.bind(this)
		},
		EstadoCivil: {
			res: $resource('/api/estadoscivis/:id.json'),
			params: {
				sort: 'descricao'
			},
			page: 1,
			get: function(params) {
				return this.model.EstadoCivil.res.get((params)?params:this.model.EstadoCivil.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.EstadoCivil.res.save(params).$promise;
			}.bind(this)
		},
		Texto: {
			res: $resource('/api/textos/:id.json'),
			params: {
				sort: 'id'
			},
			page: 1,
			get: function(params) {
				return this.model.Texto.res.get((params)?params:this.model.Texto.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.Texto.res.save(params).$promise;
			}.bind(this)
		},
		BoletoNovo: {
			res: $resource('/api/boletosnovos/:id.json'),
			params: {
				sort: '-data_vencimento'
			},
			page: 1,
			get: function(params) {
				return this.model.BoletoNovo.res.get((params)?params:this.model.BoletoNovo.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.BoletoNovo.res.save(params).$promise;
			}.bind(this),
			del: function(id) {
				return this.model.BoletoNovo.res.save(
					{
						id: id,
						situacao_id: 4,
						exc_user_id: DataService.Usuario.id,
						exc_data: new Date()
					}
				).$promise;
				
			}.bind(this)
		},
		BoletoAntigo: {
			res: $resource('/api/boletosantigos/:id.json'),
			params: {
				sort: '-data_vencimento'
			},
			page: 1,
			get: function(params) {
				return this.model.BoletoAntigo.res.get((params)?params:this.model.BoletoAntigo.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.BoletoAntigo.res.save(params).$promise;
			}.bind(this)
		},
		Agencia: {
			res: $resource('/api/agencias/:id.json'),
			params: {
				sort: 'nome'
			},
			page: 1,
			get: function(params) {
				return this.model.Agencia.res.get((params)?params:this.model.Agencia.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.Agencia.res.save(params).$promise;
			}.bind(this)
		},
		Conta: {
			res: $resource('/api/contas/:id.json'),
			params: {
				sort: 'nome'
			},
			page: 1,
			get: function(params) {
				return this.model.Conta.res.get((params)?params:this.model.Conta.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.Conta.res.save(params).$promise;
			}.bind(this),
			del: function(id) {
				return this.model.Conta.res.delete(
					{
						id: id
					}
				).$promise;
			}.bind(this)
		},
		Sacado: {
			res: $resource('/api/sacados/:id.json'),
			params: {
				sort: 'nome_razaosocial'
			},
			page: 1,
			get: function(params) {
				return this.model.Sacado.res.get((params)?params:this.model.Sacado.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.Sacado.res.save(params).$promise;
			}.bind(this)
		},
		Documento: {
			res: $resource('/api/documentos/:id.json'),
			params: {
				sort: 'id'
			},
			page: 1,
			get: function(params) {
				return this.model.Documento.res.get((params)?params:this.model.Documento.params).$promise;
			}.bind(this),
			save: function(params) {
				return this.model.Documento.res.save(params).$promise;
			}.bind(this)
		},
		Log: {
			res: $resource('/api/logs/:id.json'),
			params: {
				sort: 'data'
			},
			page: 1,
			get: function(params) {
				return this.model.Log.res.get((params)?params:this.model.Log.params).$promise;
			}.bind(this)
		},
		Retorno: {
			res: $resource('/api/retornos/:id.json'),
			params: {
				sort: 'data'
			},
			page: 1,
			get: function(params) {
				return this.model.Retorno.res.get((params)?params:this.model.Retorno.params).$promise;
			}.bind(this),
			del: function(id) {
				return this.model.Retorno.res.delete(
					{
						id: id
					}
				).$promise;
			}.bind(this)
		},
		RetornoBaixa: {
			res: $resource('/baixa/:id.json'),
			get: function(params) {
				return this.model.RetornoBaixa.res.get(params).$promise;
			}.bind(this)
		},
		Usuario: {
			res: $resource('/api/usuarios/:id.json'),
			params: {
				sort: 'username',
				populate: 'Grupo'
			},
			page: 1,
			get: function(params) {
				return this.model.Usuario.res.get((params)?params:this.model.Usuario.params).$promise;
			}.bind(this),
			save: function(data) {
				if (data.id) {
					return this.model.Usuario.res.save(
						{
							id: data.id
						},
						data
					).$promise;
				} else {
					return this.model.Usuario.res.save(data).$promise;
				}
			}.bind(this),
		},
		Grupo: {
			res: $resource('/api/grupos/:id.json'),
			params: {
				sort: 'nome'
			},
			page: 1,
			get: function(params) {
				return this.model.Grupo.res.get((params)?params:this.model.Grupo.params).$promise;
			}.bind(this),
			save: function(data) {
				this.model.Grupo.res.save(data);
			}.bind(this),
		},
		Auth: {
			res: $resource('/auth.json'),
			auth: function() {
				return this.model.Auth.res.get().$promise;
			}.bind(this)
		},
		BuscaCEP: {
			res: $resource('http://apps.widenet.com.br/busca-cep/api/cep/:cep.json'),
			get: function(params) {
				return this.model.BuscaCEP.res.get(params).$promise;
			}.bind(this),			
		},
		Bootswatch: {
			res: $resource('http://api.bootswatch.com/3/'),
			get: function() {
				return this.model.Bootswatch.res.get().$promise;
			}.bind(this),
		},
	};

}]);