<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	public $uses = array('Api.Log');
	
	public $components = array(
		'Auth' => array(
			'loginAction' => array(
				'plugin' => 'admin',
				'controller' => 'Usuario',
				'action' => 'login'
			),
			'authError' => 'Você não tem permissão!',
			'authenticate' => array(
				'Form' => array(
					'userModel'=>'Api.Usuario',
					'fields' => array(
						'username' => 'username',
						'password' => 'password'
					)
				)
			)
		)
	);
		
	public function beforeFilter() {
		//pr($this->action);
		//$method = $this->request->isPost();
		$controller = $this->name;
		$action = $this->action;
		$method = true;
		if (($action != 'index' or $action != 'view') and $this->Auth->user()) {
			
			
			$log = array(
				'controller' => $controller,
				'action' => $action,
				'data' => date('Y-m-d H:i'),
				'dados' => json_encode($this->request->data),
				'usuario_id' => $this->Auth->user('id')
			);
			
			//$this->Log->save($log);
			
		}
	}

}
